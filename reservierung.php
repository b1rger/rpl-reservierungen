<!--
@license Copyright 2011 Birger Schacht
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Reservierungen</title>
<link rel="stylesheet" type="text/css" href="stylealloc.css" />
<link rel="stylesheet" type="text/css" href="cal.css" />
<script type="text/javascript" src="cal.js"></script> 

</head>
<body>
<?php
$message = "";
$edv = " - Please mailto contact@example.org";

if (isset($_POST['submit'])) {
	$fdate = strtotime($_POST['Fdate']);
	$fdate += ($_POST['fhour']*3600);
	$tdate = strtotime($_POST['Tdate']);
	$tdate += ($_POST['thour']*3600);

	if (testinput($fdate, $tdate, $_POST['name'], $_POST['email'], $_POST['comment'])) {
		if (testtimeslice($fdate, $tdate, $_POST['item'])) {
			$pkey = genpkey();
			if (reserve($fdate, $tdate, $_POST['item'], $_POST['name'], $_POST['email'], $_POST['comment'], $pkey)) {
				$message .= "Schluessel zum Loeschen der Reservierung: ".$pkey;
			} else {
				$message .= "\nSomething went wrong reserving the item.".$edv;
			}
		}
	}

}
if (isset($_POST['delsub'])) {
	global $message;
	global $edv;
	if (isset($_POST['pkey'])) {
		$link = dbcon();
		if ($link) {
			$query = "DELETE FROM reservations WHERE pkey=\"".$_POST['pkey']."\"";
			$result = mysql_query($query);
			if (!$result) {
				$message .= "\nError: ".mysql_error().$edv;
			}
		} else {
			$message .= "\nCould not connect to database.".$edv;
		}
		mysql_close($link);
	} else {
		$message .= "\nYou have to provide a key.";
	}
}



function testinput($fdate, $tdate, $name, $email, $comment) {
	global $message;
	if ($fdate >= $tdate) {
		$message .= "\nAnfang der Reservierung sollte nicht gleich oder spaeter sein als Ende der Reservierung?!";
		return false;
	}
	if (empty($name)) {
		$message .= "\nBitte Namen angeben.";
		return false;
	}
	if (empty($email)) {
		$message .= "\nBitte Email angeben.";
		return false;
	}
	if(!preg_match('/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/',$email)) {
		$message .= "\nEmail nicht gueltig.";
		return false;
	}
	if(!preg_match('/^[-_ 0-9a-zA-Z]*$/i',$name)) {
		$message .= "\nName nicht in gueltigem Format.";
		return false;
	}
	if(!preg_match('/^[-_ 0-9a-zA-Z]*$/i',$comment)) {
		$message .= "\nComment nicht im gueltigen Format.";
		return false;
	}

	return true;
}
/*
function testtime($fdate, $tdate, $item) {
	global $message;
	global $edv;
	$link = dbcon();
	if ($link) {
		$query = "SELECT fromstamp, tostamp, name FROM reservations WHERE item=$item";
		$result = mysql_query($query);
		if (!$result) {
			$message .= "\nError: ".mysql_error().$edv;
			return false;
		} else {
			while ($reservation = mysql_fetch_object($result)) {
				if (($reservation->fromstamp < $fdate && $reservation->tostamp > $fdate) | ($reservation->fromstamp < $tdate && $reservation->tostamp > $tdate)) {
					$message .= "Reservierung ueberdeckt sich mit einer anderen Reservierung.";
					return false;
				}
			}
		} 
	} else {
		$message .= "\nCould not connect to db while trying to test timeslice.".$edv;
		return false;
	}
}*/


function reserve($fdate, $tdate, $item, $name, $email, $comment, $pkey) {
	global $message;
	$link = dbcon();
	if ($link) {
		$query = "INSERT INTO reservations (fromstamp, tostamp, item, name, email, comment, pkey, tstamp) VALUES ($fdate, $tdate, $item, \"$name\", \"$email\", \"$comment\", \"$pkey\", current_date)";
		$result = mysql_query($query);
		if (!$result) {
			$message .= "\nError: ".mysql_error().$edv;
			return false;
		}
	} else {
		$message .= "\nCould not connect to db while trying to register reservation.".$edv;
		return false;
	}
	return true;
	mysql_close($link);

}

function genpkey() {
	$gen_key = "";
	$max_char = 10;
	$alpha = "A B C D E F G H I J K L M N P Q R S T U V W X Y Z";
	$alpha = explode(" ", $alpha);
	$base = "";
	for($i = 1; $i<$max_char; $i++) {
		$base .= rand(0,1)." ";
	}
	$base = explode(" ", $base);
	$count = count($base);
	for($i = 0; $i <$count; $i++) {
		if($base[$i] == 0) { 
			$gen_key .= rand(0, 9); 
		} else  {
			$gen_key .= $alpha[rand(0, 24)];
		}
	}
	$link = dbcon();
	if ($link) {
		$query = "SELECT pkey FROM reservations";
		$result = mysql_query($query);
		if (!$result) {
			return false;
		} else {
			while ($reservation = mysql_fetch_object($result)) {
				if ($reservation->pkey == $gen_key) {
					$gen_key = genpkey();
				}
			}
		}
	}
	mysql_close($link);
	return $gen_key;
}



function testtimeslice ($fdate, $tdate, $item) {
	global $message;
	global $edv;
	$link = dbcon();
	if ($link) {
		$query = "SELECT fromstamp, tostamp, name FROM reservations WHERE item=$item";
		$result = mysql_query($query);
		if (!$result) {
			mysql_close($link);
			return false;
		} else {
			while ($reservation = mysql_fetch_object($result)) {
				//echo "blub";
				if ($fdate >= $reservation->fromstamp && $fdate <= $reservation->tostamp) {
					$message .= "\n Der Beginn der Reservierung liegt in der Reservierungszeit von " . $reservation->name;
					mysql_close($link);
					return false;
				}
				if ($tdate >= $reservation->fromstamp && $tdate <= $reservation->tostamp) {
					$message .= "\n Das Ende der Reservierung liegt in der Reservierungszeit von " . $reservation->name;
					mysql_close($link);
					return false;
				}
			}
		}
	} else {
		mysql_close($link);
		return false;
	}
	return true;
}

function dbcon() {
	$link = mysql_connect('localhost', 'dbusername', 'dbpassword');
	if (!$link) {
		$message = "Could not connect: ". mysql_error();
		return false;
	} else {
		if (!mysql_select_db('alloc')) {
			$message = "Could not use Database:". mysql_error();
			return false;
		}
	}
	return $link;
}

if (!empty($message)) {
	echo "<h2 id=\"message\">$message</h2>";
}

//print_r($_POST);
?>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="alloc">
Item:<br />
<table id="itemtable">
<tr>
<?php
global $message, $edv;
//mysql_connect('localhost', 'alloc', '4igU1N5h23givshaIDdUjriWdLQ8rQJ4') OR die ("Could not connect to SQL Server" . mysql_error());
//mysql_select_db("alloc") OR die ("Could not select Database" . mysql_error());
$link = dbcon();
$count = 1;
if (!$link) {
	$message .= "\nCould not connect to db server to list items.".$edv;
} else {
	$query = "SELECT name,id,descr FROM items WHERE hide=false";
	$result = mysql_query($query);
	if (!$result) {
		echo "No items to show!";
	} else {
		while ($item = mysql_fetch_object($result)) {
			if (($count-1) % 3 == 0) {
				echo "</tr><tr><td width=300><input type=\"radio\" name=\"item\" value=\"$item->id\" alt=\"$item->descr\"/>$item->name [$item->descr]</td>\n";
			} else {
				echo "<td width=300><input type=\"radio\" name=\"item\" value=\"$item->id\" alt=\"$item->descr\"/>$item->name [$item->descr]</td>\n";
			}
			$count++;
		}
	}
}
mysql_close($link);
?>
</tr>
</table>
<br />
<br />
<br />
<table>
<tr>
<td>Von:</td>
<td>
<input name="Fdate" type="hidden">
<input type=button value="Datum Auswaehlen" onclick="displayDatePicker('Fdate',  this);">
<!--</td>
</tr>
<tr>
<td>-->
<label for="fhour">Stunde</label>
<!--</td>
<td>-->
<select size="1" name="fhour">
<?php for ($i=1; $i<=24; $i++) { echo "<option>$i</option>"; } ?>
</select>
</td>
</tr>
<tr>
<td>Bis:</td>
<td>
<input name="Tdate" type="hidden">
<input type=button value="Datum Auswaehlen" onclick="displayDatePicker('Tdate',  this);">
<!--</td>
</tr>
<tr>
<td>-->
<label for="thour">Stunde</label>
<!--</td>
<td>-->
<select size="1" name="thour">
<?php for ($i=1; $i<=24; $i++) { echo "<option>$i</option>"; } ?>
</select>
</td>
</tr>
<tr>
<td>Name (keine Umlaute, Sonderzeichen ...):</td>
<td><input type="text" name="name" size=20 maxlength=60 value="" /></td>
</tr>
<tr>
<td>Email:</td>
<td><input type="text" name="email" size=20 maxlength=60"></td>
</tr>
<tr>
<td>Anmerkung (keine Umlaute, Sonderzeichen ...):</td>
<td><input type="text" name="comment" size=60 maxlength=100"></td>
</tr>
</table>
<input type="submit" value="Submit" name="submit"> 
</form>
</form>
<hr>
Reservierung loeschen:
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="alloc">
Reservierungsnummer: <input type="text" name="pkey" size="10">
<input type="submit" value="Submit" name="delsub">
</form>
<hr>
Reservierungen (ab jetzt):
<?php

echo "<table id=\"restable\" width=\"850px\" id=\"reslist\"><tr><td>Von/Bis</td><td>Name</td><td>Email</td><td>Was</td><td>Comment</td><td>Reserviert am</td></tr>";
printres();
echo "</table>";

function printres() {
	global $message;
	global $edv;
	$link = dbcon();
	if (!$link) {
		$message .= "Could not connect to Database".$edv;
	} else {
		$query = "SELECT fromstamp, tostamp, email, comment, tstamp, reservations.name as name, items.name as item, descr FROM reservations, items WHERE reservations.item = items.id";
		$result = mysql_query($query);
		if (!$result) {
			return false;
		} else {
			while ($reservation = mysql_fetch_object($result)) {
				if ($reservation->tostamp > date("U")) {
					echo "<tr><td width=\"350\">".date("j. M. Y - H:00", $reservation->fromstamp)."<br> bis <br>".date("j. M. Y - H:00", $reservation->tostamp)."</td>
						<td width=\"200\">$reservation->name</td>
						<td width=\"200\"><a href=\"mailto:$reservation->email\">$reservation->email</a></td>
						<td width=\"200\">$reservation->item <br> $reservation->descr</td>
						<td width=\"300\">$reservation->comment</td>
						<td width=\"150\">$reservation->tstamp</td><tr>";
				}
			}
		}
	}
	mysql_close($link);
}

?>
</body>
</html>
