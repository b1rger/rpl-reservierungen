*Please don't use this! Its bitrotten and its here only for archival purposes*

A simple webinterface for booking resources

# Database

```
mysql> use alloc;
mysql> show tables;
+-----------------+
| Tables_in_alloc |
+-----------------+
| items           |
| reservations    |
+-----------------+

mysql> describe reservations;
+-----------+--------------+------+-----+---------+----------------+
| Field     | Type         | Null | Key | Default | Extra          |
+-----------+--------------+------+-----+---------+----------------+
| id        | int(11)      | NO   | PRI | NULL    | auto_increment |
| fromstamp | int(11)      | YES  |     | NULL    |                |
| tostamp   | int(11)      | YES  |     | NULL    |                |
| item      | int(11)      | YES  | MUL | NULL    |                |
| name      | varchar(40)  | YES  |     | NULL    |                |
| email     | varchar(40)  | YES  |     | NULL    |                |
| comment   | varchar(200) | YES  |     | NULL    |                |
| tstamp    | date         | YES  |     | NULL    |                |
| pkey      | varchar(10)  | YES  |     | NULL    |                |
+-----------+--------------+------+-----+---------+----------------+

mysql> describe items;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(30)  | YES  |     | NULL    |                |
| descr | varchar(200) | YES  |     | NULL    |                |
| hide  | tinyint(1)   | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
```
